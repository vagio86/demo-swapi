const express = require('express');
const bodyParser = require('body-parser');
var path = require('path');
const routes = require('./routes/v1/');

const port = 3000;
const app = express();

//Se define el motor de plantillas, para el demo se utiliza ejs
app.set('views', path.join(__dirname, 'views'));
app.set("view engine","ejs");
app.use(express.static('./public'));

//Esta instruccion se encarga de parsear las respuestas a un formato json
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//Se hace referencia al archivo encargado de procesar las rutas de la app
routes(app);

//Si se ingresa una url que no esta registrada, se devuelve una respuesta not found
app.use((req, resp)=>{
    resp.status(404).send("NOT FOUND");
});

//Se inicia el servidor en el puerto deseado
app.listen(port, ()=>{
    console.log('runing on port ' + port);
});