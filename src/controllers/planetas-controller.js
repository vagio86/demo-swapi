const promise = require('request-promise');


/*
    Se recupera la informacion de los planetas y se pasa el resultado para obtener los nombres de los residentes
*/
const getPlanetasResidentes = (req, resp) =>{
    promise({
        uri: 'http://swapi.dev/api/planets/',
          json: true
    })
    .then((data) => {
        let planetas = data.results;
        getPlanetas(planetas)
        .then(data => {
            resp.send(JSON.stringify(data));
        });
        
    })
    .catch((err) => {
        console.log(err);
    })
}

/*
Se obtienen las urls de los residentes para cada planeta y se van almacenando en un arreglo con los nombres de los planetas
*/
function getPlanetas(planetas) {
	const result = {};
	return Promise.all(
		planetas.map(planeta => {
			
			return getNames(planeta.residents)
				.then(residents => {
					result[planeta.name] = residents
				})
		})
	)
	.then(() => {
		return result;
	})
}

/* 
se recuperan los nombres de los residentes segun la url que se tenga registrada en el planeta
*/
function getNames(param) {
	return Promise.all(
		param.map(residente => {
			return promise({
				uri: residente,
			  	json: true
			})
		})
	).then(residents => {
		const residentNames = residents.map(resident => {
			return resident.name
		})
		return residentNames;
	}).catch (err => {
		console.log(err);
	})
}



module.exports = {
    getPlanetasResidentes
}