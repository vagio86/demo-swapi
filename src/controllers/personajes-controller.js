const axios = require("axios");
const promise = require('request-promise');

/*
    Se obtienen todos los personajes
    El api regresa un total de 10 registros por pagina, 
    por lo tanto para recuperar los 50 registros que se solicitan, se tiene que consumir el api 5 veces
    Se pasa el parametro ordenar para el caso en que se quiera ordenar la informacion por nombre, altura o peso
*/
function getAllPersonajes (ordenar)  {

    let people = [];
    
    return axios("https://swapi.dev/api/people/")
        .then(response => {
            const paginas = 5;
            let promises = [];
            for (let i = 2; i <= paginas; i++) {
                promises.push(axios(`https://swapi.dev/api/people?page=${i}`));
            }
            return Promise.all(promises);
        })
        .then(response => {
            people = response.reduce((acc, data) => [...acc, ...data.data.results], people);

            if(ordenar=='nombre'){
                people.sort(function(a, b) {
                    return sortStrings(a.name, b.name);
                })
            }

            if(ordenar=='peso'){
                people.sort(function(a, b) {
                    return sortNumber(a.mass, b.mass);
                })
            }

            if(ordenar=='altura'){
                people.sort(function(a, b) {
                    return sortNumber(a.height, b.height);
                })
            }

            return people;
        })
        .catch(error => console.log("Error al procesar la solicitud"));
}

//Funcion para ordenar un arreglo por un parametro de tipo string
function sortStrings(a, b) {
    a = a.toLowerCase();
    b = b.toLowerCase();
    return (a < b) ? -1 : (a > b) ? 1 : 0;
}

//funcion para ordenar un arreglo por un parametro numerico
function sortNumber(a,b) {
      return parseInt(a) - parseInt(b);
}

//metodo para recuperar un personaje por nombre y visualizar su informacion en una plantilla
const getPersonajeByName = (req, resp) =>{
    resp.header("Content-type", "application/json")
    const{nombre} = req.params;
    
    promise({
        uri: `https://swapi.dev/api/people/?search=${nombre}`,
        json: true
    })
    .then((data) => {
        resp.header("Content-type", "text/html")
        resp.render('pages/personaje', {
			data: data.results
		});
    })
    .catch((err) => {
        resp.send(JSON.stringify("error"));
    })
}

/*
Se llama al metodo para obtener todos los personajes
Debido a que se realizan varios llamados al API se utiliza un metodo async para recuperar la informacion
*/
const getPersonajes = async (req, resp) => {
    const {ordenar}=req.query;
    const result = await getAllPersonajes(ordenar);
    resp.send(result);  
};

module.exports = {
    getPersonajes,
    getPersonajeByName
}
