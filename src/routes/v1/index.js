//se importan las funciones necesarias, las cuales seran llamadas segun la ruta
const personajesController = require('../../controllers/personajes-controller');
const planetasController = require('../../controllers/planetas-controller');

// se definen las rutas y los metodos que consumen
const createRoutes = (app) =>{
    app.get('/personajes',(req, resp)=>{ personajesController.getPersonajes(req, resp); });
    app.get('/personaje/:nombre', personajesController.getPersonajeByName)
    app.get('/residentes', planetasController.getPlanetasResidentes);
};

module.exports = createRoutes;